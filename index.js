console.log("hrmmm?")

// Fetch keyword
	// Syntax:
				// options - methods, header, body(textarea), authorization
		// fetch ('url', {options});

	// GET post data
	// https://jsonplaceholder.typicode.com/posts

	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => {
		// console.log(response.json());
		// return response;
		return response.json();
	// naka-object so we need to convert in json by .json()
	}).then(result => {
		console.log(result);
		showPosts(result);
	})
	// .catch(error => response.send(error))


//SHOW post
		// create a function that will let us show the posts from our API

	const showPosts = (posts) => {
		let entries = ``;
		// ^containers for the fetched database

		// need identifiers (id)
		posts.forEach((post) => {
			// concatinate posted entries
			entries += `
			<div id = "post-${post.id}">
				<h3 id = "post-title-${post.id}">${post.title}</h3>
				<p id = "post-body-${post.id}">${post.body}</p>
				<button onclick = "editPost(${post.id})">Edit</button>
				<button onclick = "deletePost(${post.id})">Delete</button>
			</div>
			`
		})
		// target id where to post by document.querySelector
		document.querySelector("#div-post-entries").innerHTML = entries;
	}


//POST data on our API
		// first we need to target the form for creating/adding post
	document.querySelector('#form-add-post')
	.addEventListener("submit", (event) => {
		// to change the auto reload of the submit method
		event.preventDefault();

		//post method
			// will return the newly created document
		fetch('https://jsonplaceholder.typicode.com/posts', {
			method: 'POST', //method for creating
			body: JSON.stringify({
				title: document.querySelector('#txt-title').value,
				body: document.querySelector('#txt-body').value,
				userId: 1
			}),
			headers: {
				'Content-Type' : 'application/json'
			}
		}).then(response => response.json()
		).then(result => {
			console.log(result)

			alert("Post is successfully added!")

			document.querySelector('#txt-title').value = null;
			document.querySelector('#txt-body').value = null;
		})
	})


//EDIT post 
	const editPost = (id) => {
		console.log(id);

		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		console.log(title);
		console.log(body);

		document.querySelector(`#txt-edit-title`).value = title;
		document.querySelector(`#txt-edit-body`).value = body;
		document.querySelector(`#txt-edit-id`).value = id;

		// removeAttribute will remove the declared attribute from the element
		document.querySelector(`#btn-submit-update`).removeAttribute('disabled');

		document.querySelector(`#form-edit-post`)
		.addEventListener("submit", (event) => {

			event.preventDefault();

			let id = document.querySelector("#txt-edit-id").value;
			 //^ captures id to be edited

			// use template literals(``) to be dynamic, targets id specified
			fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
				method: 'PUT', //method for updating
				headers: {
					'Content-Type' : 'application/json'
				 },
				body: JSON.stringify({
					title: document.querySelector(`#txt-edit-title`).value,
					id: id, //variable with the contained id to be edited
					body: document.querySelector(`#txt-edit-body`).value,
					userId: 1
				 })
			}).then(response => response.json()
			).then(result => {
				console.log(result);

				alert("The post is successfully updated!");

				document.querySelector(`#txt-edit-title`).value = null;
				document.querySelector(`#txt-edit-body`).value = null;
				document.querySelector(`#txt-edit-id`).value = null;

				document.querySelector(`#btn-submit-update`).setAttribute('disabled', true);
			})
		})
	}


//DELETE POST
	const deletePost = (id) => {
		// let posts = document.querySelector(`#div-post-entries`);
		// console.log(posts)

		// console.log(`${id}`)
		// fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		// 	method: 'DELETE', //method for deleting
		// 	headers: {
		// 		'Content-Type': 'application/json'
		// 	}
		// }).then(response => response.json()
		// ).then(result => {
		// 	console.log(result)
		// })
		

		document.querySelector(`#post-${id}`).remove();
		// console.log(del);
		// del = null;
		// console.log(del);

	}